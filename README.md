# Docker Proxy with HTTPS Support

## Commands

### check if the nginx tmpl is running:

`docker exec nginx nginx -s reload`

With this command you can see if errors are thrown which are not shown in docker log.

### CERTS Folder

Create one central certs folder for all your environments. In this folder you can create working certs with this command:

`sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout default.key -out default.crt`

The files must be named like the url. For example local.docker-order-manager.de.key and local.docker-order-manager.de.crt.

### Show nginx configuration

`docker exec -it nginx-proxy cat /etc/nginx/conf.d/default.conf`

'nginx-proxy' is the containers name.

### show certs folder directly from docker

`docker exec 889fc2c91f91 ls -l /etc/nginx/certs`

## install way

- clone directory
- create certs directory on your harddisk and create the certs with the command `sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout default.key -out default.crt`
- `docker-compose up -d` in docker-proxy-ssl directory

### example for container config

~~~~
ordermanager:
  image: rotd/docker-base-php7-proxy-cron-ssl
  container_name: ordermanager
  #extra_hosts:
  # - "local.klotzaufklotz.de:172.17.0.5"
  # - "local.docker-shopware-customizer.de:172.17.0.7"
  environment:
   - VIRTUAL_HOST=local.docker-order-manager.de
   # phpmyadmin password
   - PHPMYADMIN_PW=slim
   - ROOT_DIR=public/
   # DB Values
   - DB_USER=slim
   - DB_PASSWORD=slim
   - DB_DATABASE=slim
   - DB_PORT=tcp://172.17.0.11:3306
   - DB_HOST=172.17.0.11
  volumes:
   - ./:/var/www/html
   - /Library/WebServer/Documents/_docker/_shared/certs:/etc/apache2/ssl
  expose:
   - "80"
   - "443"  
~~~~


